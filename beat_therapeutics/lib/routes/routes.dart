class ModuleRoutes {
  static String errorPage = '/error';

  static String initialPage = '/';

  static String homePage = '/homepage';

  static String privacyPolicyPage = '/privacyPolicy';
}
