import 'package:beat_therapeutics/errorPage/errorpage_view.dart';
import 'package:beat_therapeutics/homeView/home_view.dart';
import 'package:beat_therapeutics/privacyPolicyPage/privacypolicy_view.dart';
import 'package:beat_therapeutics/routes/routes.dart';
import 'package:get/get.dart';

class AppRouting {
  get pages {
    return [
      GetPage(
          name: ModuleRoutes.errorPage,
          page: () => const ErrorPage(),
          transitionDuration: const Duration(milliseconds: 500),
          transition: Transition.fadeIn),

      GetPage(
        name: ModuleRoutes.initialPage,
        page: () => const HomepageView(),
        transitionDuration: const Duration(milliseconds: 500),
        transition: Transition.fadeIn,
      ),

      GetPage(
        name: ModuleRoutes.homePage,
        page: () => const HomepageView(),
        transitionDuration: const Duration(milliseconds: 500),
        transition: Transition.fadeIn,
      ),
      GetPage(
        name: ModuleRoutes.privacyPolicyPage,
        page: () => const PrivacyPolicyPage(),
        transitionDuration: const Duration(milliseconds: 500),
        transition: Transition.fadeIn,
      )
      //Call
    ];
  }
}
