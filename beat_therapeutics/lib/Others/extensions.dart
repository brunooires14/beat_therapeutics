import 'dart:math' as math;

import 'package:flutter/material.dart';

class FirstTriangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width, size.height);
    path.lineTo(0.0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(FirstTriangleClipper oldClipper) => false;
}

class SecondTriangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width, 0.0);
    path.lineTo(size.width, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(SecondTriangleClipper oldClipper) => false;
}

class QuarterCircleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width, 0);
    path.arcToPoint(Offset(0, size.height),
        radius: Radius.elliptical(size.width, size.height));
    path.close();
    return path;
  }

  @override
  bool shouldReclip(QuarterCircleClipper oldClipper) => false;
}

class ContactsArcClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Offset center = Offset(size.width / 2, -size.width / 1.6);
    var rect = Rect.fromCircle(center: center, radius: size.width * 0.7071);
    final path = Path();
    path.lineTo(size.width, 0);
    path.addArc(rect, 0, math.pi);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(ContactsArcClipper oldClipper) => true;
}

extension GlobalKeyExtension on GlobalKey {
  Offset? get globalPaintBounds {
    final renderObject = currentContext?.findRenderObject() as RenderBox?;
    final position = renderObject?.localToGlobal(Offset.zero);

    if (position != null) {
      return position;
    } else {
      return null;
    }
  }
}
