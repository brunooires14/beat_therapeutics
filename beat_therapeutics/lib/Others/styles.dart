import 'package:flutter/material.dart';

class Styles {
  Color beatDarkBlue = const Color(0xff3A7193);
  Color beatLightBlue = const Color(0xff5ABFC9);
  ButtonStyle drawerButtons(TabController tabController) {
    return ButtonStyle(
        backgroundColor: tabController.index == 0
            ? WidgetStateProperty.all(Colors.white)
            : WidgetStateProperty.all(beatDarkBlue),
        foregroundColor: tabController.index == 0
            ? WidgetStateProperty.all(beatDarkBlue)
            : WidgetStateProperty.all(Colors.white),
        textStyle: WidgetStateProperty.all(const TextStyle(
            fontFamily: 'Inter', fontSize: 18, fontWeight: FontWeight.w600)),
        shadowColor: WidgetStateProperty.all(Colors.transparent));
  }
}
