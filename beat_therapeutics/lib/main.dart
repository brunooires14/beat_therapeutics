import 'package:beat_therapeutics/errorPage/errorpage_view.dart';
import 'package:beat_therapeutics/routes/app_routing.dart';
import 'package:beat_therapeutics/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const BeatTherapeuticsApp());
}

class BeatTherapeuticsApp extends StatelessWidget {
  const BeatTherapeuticsApp({super.key});

  _getErrorPage() {
    return GetPage(
        name: ModuleRoutes.errorPage,
        page: () => const ErrorPage(),
        transitionDuration: const Duration(milliseconds: 500),
        transition: Transition.fadeIn);
  }

  _getPages() {
    return AppRouting().pages;
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'Beat Therapeutics',
        initialRoute: '/',
        getPages: _getPages(),
        debugShowCheckedModeBanner: false,
        unknownRoute: _getErrorPage());
  }
}
