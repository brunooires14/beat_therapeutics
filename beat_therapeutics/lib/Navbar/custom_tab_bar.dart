import 'package:flutter/material.dart';

class CustomTabBar extends StatefulWidget {
  const CustomTabBar(
      {super.key,
      required this.controller,
      required this.tabs,
      required this.key1,
      required this.key2,
      required this.key3,
      required this.key4,
      required this.key5,
      this.indicatorColor,
      this.selectedLabelColor,
      this.selectedLabelStyle,
      this.unselectedLabelColor,
      this.unselectedLabelStyle});

  final TabController controller;
  final List<Widget> tabs;
  final GlobalKey key1;
  final GlobalKey key2;
  final GlobalKey key3;
  final GlobalKey key4;
  final GlobalKey key5;
  final Color? indicatorColor;
  final Color? selectedLabelColor;
  final TextStyle? selectedLabelStyle;
  final Color? unselectedLabelColor;
  final TextStyle? unselectedLabelStyle;

  @override
  State<CustomTabBar> createState() => _CustomTabBarState();
}

class _CustomTabBarState extends State<CustomTabBar> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double tabBarScaling = screenWidth > 1400
        ? 0.4
        : screenWidth > 1100
            ? 0.5
            : 0.67;
    return SizedBox(
      height: double.infinity,
      width: screenWidth * tabBarScaling,
      child: TabBar(
          controller: widget.controller,
          labelColor: widget.selectedLabelColor ?? Colors.white,
          automaticIndicatorColorAdjustment: false,
          indicatorColor: widget.indicatorColor ?? Colors.transparent,
          dividerColor: Colors.transparent,
          dividerHeight: 0,
          labelStyle: widget.selectedLabelStyle ??
              const TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
          unselectedLabelColor: widget.unselectedLabelColor ?? Colors.black,
          unselectedLabelStyle: widget.unselectedLabelStyle ??
              const TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
          tabs: widget.tabs,
          onTap: (index) {
            setState(() {
              switch (index) {
                case 0:
                  Scrollable.ensureVisible(widget.key1.currentContext!,
                      duration: const Duration(milliseconds: 1000));
                  break;
                case 1:
                  Scrollable.ensureVisible(widget.key2.currentContext!,
                      duration: const Duration(milliseconds: 1000));
                  break;
                case 2:
                  Scrollable.ensureVisible(widget.key3.currentContext!,
                      duration: const Duration(milliseconds: 1000));
                  break;
                case 3:
                  Scrollable.ensureVisible(widget.key4.currentContext!,
                      duration: const Duration(milliseconds: 1000));
                  break;
                case 4:
                  Scrollable.ensureVisible(widget.key5.currentContext!,
                      duration: const Duration(milliseconds: 1000));
                  break;
                default:
              }
            });
          }),
    );
  }
}

class CustomTab extends StatelessWidget {
  const CustomTab({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Tab(
        child: Text(
      title,
    ));
  }
}
