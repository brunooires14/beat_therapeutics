import 'package:beat_therapeutics/Navbar/custom_tab_bar.dart';
import 'package:flutter/material.dart';
import 'package:simple_drawer/simple_drawer.dart';

class ResponsiveTopNavigationBar extends StatefulWidget
    implements PreferredSizeWidget {
  const ResponsiveTopNavigationBar({
    super.key,
    required this.navbarItems,
    required this.key1,
    required this.key2,
    required this.key3,
    required this.key4,
    required this.key5,
    required this.context,
    required this.screenHeight,
    required this.onTapIconButton,
    required this.drawerOpen,
    this.imageContainer,
    this.tabController,
    this.height,
    this.backgroundColor,
    this.indicatorColor,
    this.selectedLabelColor,
    this.selectedLabelStyle,
    this.unselectedLabelColor,
    this.unselectedLabelStyle,
  });

  final List<CustomTab> navbarItems;
  final GlobalKey key1;
  final GlobalKey key2;
  final GlobalKey key3;
  final GlobalKey key4;
  final GlobalKey key5;
  final BuildContext context;
  final double screenHeight;
  final VoidCallback onTapIconButton;
  final bool drawerOpen;
  final Widget? imageContainer;
  final TabController? tabController;
  final double? height;
  final Color? backgroundColor;
  final Color? indicatorColor;
  final Color? selectedLabelColor;
  final TextStyle? selectedLabelStyle;
  final Color? unselectedLabelColor;
  final TextStyle? unselectedLabelStyle;

  @override
  State<ResponsiveTopNavigationBar> createState() =>
      _ResponsiveTopNavigationBarState();
  @override
  Size get preferredSize => Size.fromHeight(screenHeight);
}

class _ResponsiveTopNavigationBarState extends State<ResponsiveTopNavigationBar>
    with SingleTickerProviderStateMixin {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  late TabController tabController;
  late double screenWidth;
  late double screenHeight;

  @override
  void initState() {
    super.initState();
    tabController =
        TabController(length: widget.navbarItems.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;
    return Container(
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(color: widget.backgroundColor),
      width: screenWidth,
      height: widget.height,
      child: screenWidth > 900 ? desktopView() : mobileView(),
    );
  }

  Widget desktopView() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                width: 80,
              ),
              widget.imageContainer ?? Container()
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CustomTabBar(
                controller: widget.tabController ?? tabController,
                tabs: widget.navbarItems,
                key1: widget.key1,
                key2: widget.key2,
                key3: widget.key3,
                key4: widget.key4,
                key5: widget.key5,
                indicatorColor: widget.indicatorColor,
                selectedLabelColor: widget.selectedLabelColor,
                selectedLabelStyle: widget.selectedLabelStyle,
                unselectedLabelColor: widget.unselectedLabelColor,
                unselectedLabelStyle: widget.unselectedLabelStyle,
              ),
              const SizedBox(
                width: 40,
              ),
            ],
          )
        ]);
  }

  Widget mobileView() {
    return Stack(
      children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  widget.imageContainer ?? Container()
                ],
              ),
              Row(
                children: [
                  IconButton(
                    iconSize: 40,
                    onPressed: () {
                      if (widget.drawerOpen) {
                        SimpleDrawer.deactivate("top");
                      } else {
                        SimpleDrawer.activate("top");
                      }
                    },
                    icon: widget.drawerOpen
                        ? const Icon(Icons.close_rounded)
                        : const Icon(Icons.menu_rounded),
                    color: widget.unselectedLabelColor,
                  ),
                  const SizedBox(
                    width: 10,
                  )
                ],
              )
            ]),
      ],
    );
  }
}
