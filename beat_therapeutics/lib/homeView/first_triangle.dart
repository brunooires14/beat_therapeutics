import 'package:beat_therapeutics/Others/extensions.dart';
import 'package:flutter/material.dart';

class FirstTriangle {
  String subtitle = "Inspired by nature, perfected in the lab";
  String descriptionDesktop =
      "Founded in 2023 as a University of Porto spin-off,\nBEAT Therapeutics journey began with the discovery \nof the anti-tumoral properties of compounds derived from\na natural source, which were mostly unexplored by modern medicine.\nOur story unfolds in the pursuit of pioneering a new class of cancer\ntherapeutics and advancing the frontiers of healthcare innovation for the\nbenefit of patients.";
  String descriptionMobile =
      "Founded in 2023 as a University of Porto spin-off,BEAT Therapeutics journey began with the discovery of the anti-tumoral properties of compounds derived from a natural source, which were mostly unexplored by modern medicine. Our story unfolds in the pursuit of pioneering a new class of cancer therapeutics and advancing the frontiers of healthcare innovation for the benefit of patients.";

  Widget clipPathFirstTriangle(
      double screenHeight,
      double screenWidth,
      double titleSize,
      double subtitleSize,
      double textSize,
      double firstSpacerHeight,
      double secondSpacerHeight,
      double imageWidth,
      double imageHeight,
      double imagePosLeft,
      double imagePosTop,
      double titlePosLeft,
      double titlePosTop,
      GlobalKey tab3Key) {
    return Stack(
      children: [
        ClipPath(
          clipper: FirstTriangleClipper(),
          child: Container(
            color: const Color(0xff3A7193),
            height: screenHeight - 90,
            width: screenWidth * 0.65,
            child: Row(
              children: [
                const SizedBox(
                  width: 100,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(subtitle,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Inter',
                            fontSize: subtitleSize,
                            fontWeight: FontWeight.w500)),
                    SizedBox(height: firstSpacerHeight),
                    Text(descriptionDesktop,
                        textAlign: TextAlign.left,
                        softWrap: true,
                        maxLines: 7,
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Inter',
                            height: 1.6,
                            fontSize: textSize,
                            fontWeight: FontWeight.w200)),
                    SizedBox(key: tab3Key, height: secondSpacerHeight)
                  ],
                )
              ],
            ),
          ),
        ),
        Positioned(
          top: imagePosTop,
          left: imagePosLeft,
          child: Container(
              width: imageWidth / 1.1,
              height: imageHeight / 1.1,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/microscope.png"),
                  fit: BoxFit.cover,
                ),
              )),
        ),
        Positioned(
            top: titlePosTop,
            left: titlePosLeft,
            child: Text('who\nwe\nare'.toUpperCase(),
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.white,
                    height: 1.1,
                    fontFamily: 'BigCaslon',
                    fontSize: titleSize,
                    fontWeight: FontWeight.w500))),
      ],
    );
  }

  Widget desktop(double screenHeight, double screenWidth, GlobalKey tab3Key) {
    if (screenHeight < 1150 && screenWidth < 2000) {
      return clipPathFirstTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 90,
          screenWidth / 95,
          screenHeight / 20,
          screenHeight / 10,
          screenHeight / 2.6,
          screenHeight / 2.6,
          screenHeight / 5,
          screenHeight / 10,
          screenHeight / 8,
          screenHeight / 5,
          tab3Key);
    }
    if (screenHeight < 1150 && screenWidth > 2000) {
      return clipPathFirstTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 85,
          screenWidth / 90,
          screenHeight / 30,
          screenWidth / 40,
          screenHeight / 2.6,
          screenHeight / 2.6,
          screenHeight / 5,
          screenHeight / 10,
          screenHeight / 8,
          screenHeight / 5,
          tab3Key);
    }
    if (screenHeight > 1150 && screenHeight < 1300 && screenWidth < 2600) {
      return clipPathFirstTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 80,
          screenWidth / 85,
          screenHeight / 18,
          screenHeight / 14,
          screenHeight / 2.4,
          screenHeight / 2.4,
          screenHeight / 6.5,
          screenHeight / 10,
          screenHeight / 10,
          screenHeight / 5,
          tab3Key);
    }
    if (screenHeight > 1150 && screenHeight < 1300 && screenWidth > 2600) {
      return clipPathFirstTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 75,
          screenWidth / 80,
          screenWidth / 55,
          screenWidth / 45,
          screenHeight / 2.4,
          screenHeight / 2.4,
          screenHeight / 6.5,
          screenHeight / 10,
          screenHeight / 10,
          screenHeight / 5,
          tab3Key);
    }
    if (screenHeight > 1300 && screenHeight < 1400 && screenWidth > 2600) {
      return clipPathFirstTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 70,
          screenWidth / 75,
          screenWidth / 45,
          screenWidth / 35,
          screenHeight / 2.4,
          screenHeight / 2.4,
          screenHeight / 6.5,
          screenHeight / 10,
          screenHeight / 10,
          screenHeight / 5,
          tab3Key);
    }
    if (screenHeight > 1300 && screenHeight < 1400 && screenWidth < 2600) {
      return clipPathFirstTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 70,
          screenWidth / 75,
          screenWidth / 45,
          screenWidth / 30,
          screenHeight / 2.4,
          screenHeight / 2.4,
          screenHeight / 6.5,
          screenHeight / 10,
          screenHeight / 10,
          screenHeight / 5,
          tab3Key);
    }
    if (screenHeight > 1300 && screenHeight < 1400 && screenWidth < 2600) {
      return clipPathFirstTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 70,
          screenWidth / 75,
          screenWidth / 45,
          screenWidth / 30,
          screenHeight / 2.4,
          screenHeight / 2.4,
          screenHeight / 6.5,
          screenHeight / 10,
          screenHeight / 10,
          screenHeight / 5,
          tab3Key);
    }
    return clipPathFirstTriangle(
        screenHeight,
        screenWidth,
        screenHeight / 15,
        screenWidth / 70,
        screenWidth / 75,
        screenWidth / 45,
        screenWidth / 30,
        screenHeight / 2.4,
        screenHeight / 2.4,
        screenHeight / 6.5,
        screenHeight / 10,
        screenHeight / 10,
        screenHeight / 5,
        tab3Key);
  }

  Widget mobile(double screenWidth) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: screenWidth - 100,
              child: Text(subtitle,
                  textAlign: TextAlign.left,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      color: Colors.white,
                      fontFamily: 'Inter',
                      fontSize: 20,
                      fontWeight: FontWeight.w500)),
            ),
            const SizedBox(height: 30),
            SizedBox(
              width: screenWidth - 100,
              child: Text(descriptionMobile,
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 30,
                  style: const TextStyle(
                      color: Colors.white,
                      fontFamily: 'Inter',
                      fontSize: 17,
                      fontWeight: FontWeight.w200)),
            ),
          ],
        ),
      ],
    );
  }
}
