import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Team {
  String angela =
      "Biotech entrepreneur with a background in cancer precision medicine and a PhD in Biomedical Engineering. With over 5 years of diverse management experience, Carvalho also holds expertise in tech transfer and supporting early-stage startups. Committed to translating scientific innovations into impactful solutions that improve patient outcomes.";
  String hugo =
      "PhD in Biomedicine, MBA, entrepreneur. Deep scientific background in cancer. Over 8 years' experience as head of precision medicine testing lab at a reference oncology hospital. Over 15 years as Biotech founder (4 ventures launched). Innovation manager with track-record in transitioning innovation to industry.";
  String lucilia =
      "Associate Professor with Habilitation at the Faculty of Pharmacy of the University of Porto and an expert in drug discovery and cancer pharmacology. Saraiva expertise has led to the development of 3 candidate therapeutics, 6 patents and over 100 scientific articles.";
  String maria =
      "Full Professor of Medicinal and Organic Chemistry at the Faculty of Pharmacy, University of Lisbon. Large experience in the discovery of anticancer plant-derived compounds, through isolation and molecular derivatization. Has authored more than 130 articles and book chapters.";
  String lucio =
      "Surgical Oncologist, Head of Surgical Oncology Department and the Experimental Pathology and Therapeutics research group at IPO-Porto. Conducts research on translational oncology and is responsible researcher in 2 pre-clinical and clinical research projects.";

  Color beatDarkBlue = const Color(0xff3A7193);
  Color beatMediumBlue = const Color(0xff49AFDE);
  Color beatLightBlue = const Color(0xff5ABFC9);

  Uri emailAngela = Uri.parse("mailto:angela.carvalho@beattherapeutics.com");
  Uri emailHugo = Uri.parse("mailto:hugo.prazeres@beattherapeutics.com");
  Uri emailLucilia = Uri.parse("mailto:lucilia.saraiva@ff.up.pt");
  Uri emailMaria = Uri.parse("mailto:mjuferreira@ff.ulisboa.pt");
  Uri emailLucio = Uri.parse("mailto:lucio.santos@ipoporto.min-saude.pt");
  Uri linkedinAngela =
      Uri.parse("https://www.linkedin.com/in/angela-carvalho-phd/");
  Uri linkedinHugo =
      Uri.parse("https://pt.linkedin.com/in/hugo-prazeres-890b5830");
  Uri orcidLucilia = Uri.parse("https://orcid.org/0000-0002-9531-4939");
  Uri orcidMaria = Uri.parse("https://orcid.org/0000-0002-8742-1486");
  Uri orcidLucio = Uri.parse("https://orcid.org/0000-0002-0521-5655");

  Widget buildAngela(double screenHeight, double screenWidth) {
    return Container(
      color: beatDarkBlue,
      width: screenWidth,
      child: Stack(
        children: [
          Container(
            height: screenHeight / 2.7,
            width: screenWidth * 0.93,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(200),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth / 40,
              ),
              Container(
                  width: screenHeight / 2.7,
                  height: screenHeight / 2.7,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("images/profile_photo_angela.png"),
                      fit: BoxFit.cover,
                    ),
                  )),
              SizedBox(
                width: screenWidth / 40,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("ângela".toUpperCase(),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: beatMediumBlue,
                          fontFamily: 'BigCaslon',
                          fontSize: screenHeight / 12,
                          fontWeight: FontWeight.w500)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/email_blue.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(emailAngela);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                          const SizedBox(height: 5),
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/linkedin_blue.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(linkedinAngela);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 10),
                      Text("carvalho".toUpperCase(),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              height: 1,
                              color: beatMediumBlue,
                              fontFamily: 'BigCaslon',
                              fontSize: screenHeight / 12,
                              fontWeight: FontWeight.w500)),
                    ],
                  ),
                  SizedBox(height: screenHeight / 100),
                  Text("Co-founder & CEO",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: beatDarkBlue,
                          fontFamily: 'Inter',
                          fontSize: screenHeight / 40,
                          fontWeight: FontWeight.w700)),
                  SizedBox(height: screenHeight / 100),
                  SizedBox(
                    width: screenWidth / 1.8,
                    child: Text(Team().angela,
                        textAlign: TextAlign.left,
                        maxLines: 20,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            height: 1.5,
                            color: beatDarkBlue,
                            fontFamily: 'Inter',
                            fontSize: screenHeight / 45,
                            fontWeight: FontWeight.w300)),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildHugo(double screenHeight, double screenWidth) {
    return Container(
      color: beatDarkBlue,
      width: screenWidth,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(left: screenWidth * 0.07),
            height: screenHeight / 2.7,
            width: screenWidth * 0.93,
            decoration: const BoxDecoration(
              gradient: RadialGradient(
                colors: [Color(0xff49AFDD), Color(0xff5ABFC9)],
              ),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(200),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("hugo".toUpperCase(),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: Colors.white,
                          fontFamily: 'BigCaslon',
                          fontSize: screenHeight / 12,
                          fontWeight: FontWeight.w500)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/email_white.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(emailHugo);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                          const SizedBox(height: 5),
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/linkedin_white.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(linkedinHugo);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 10),
                      Text("prazeres".toUpperCase(),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              height: 1,
                              color: Colors.white,
                              fontFamily: 'BigCaslon',
                              fontSize: screenHeight / 12,
                              fontWeight: FontWeight.w500)),
                    ],
                  ),
                  SizedBox(height: screenHeight / 100),
                  Text("Co-founder & CBDO",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: beatDarkBlue,
                          fontFamily: 'Inter',
                          fontSize: screenHeight / 40,
                          fontWeight: FontWeight.w700)),
                  SizedBox(height: screenHeight / 100),
                  SizedBox(
                    width: screenWidth / 1.9,
                    child: Text(Team().hugo,
                        textAlign: TextAlign.left,
                        maxLines: 20,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            height: 1.5,
                            color: beatDarkBlue,
                            fontFamily: 'Inter',
                            fontSize: screenHeight / 45,
                            fontWeight: FontWeight.w300)),
                  ),
                ],
              ),
              SizedBox(
                width: screenWidth / 10,
              ),
              Container(
                  width: screenHeight / 2.7,
                  height: screenHeight / 2.7,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("images/profile_photo_hugo.png"),
                      fit: BoxFit.cover,
                    ),
                  )),
              SizedBox(
                width: screenWidth / 40,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget buildLucilia(double screenHeight, double screenWidth) {
    return Container(
      color: beatDarkBlue,
      width: screenWidth,
      child: Stack(
        children: [
          Container(
            height: screenHeight / 2.7,
            width: screenWidth * 0.93,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(200),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth / 40,
              ),
              Container(
                  width: screenHeight / 2.7,
                  height: screenHeight / 2.7,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("images/profile_photo_lucilia.png"),
                      fit: BoxFit.cover,
                    ),
                  )),
              SizedBox(
                width: screenWidth / 40,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("lucília".toUpperCase(),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: beatMediumBlue,
                          fontFamily: 'BigCaslon',
                          fontSize: screenHeight / 12,
                          fontWeight: FontWeight.w500)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/email_blue.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(emailLucilia);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                          const SizedBox(height: 5),
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/orcid_blue.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(orcidLucilia);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 10),
                      Text("saraiva".toUpperCase(),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              height: 1,
                              color: beatMediumBlue,
                              fontFamily: 'BigCaslon',
                              fontSize: screenHeight / 12,
                              fontWeight: FontWeight.w500)),
                    ],
                  ),
                  SizedBox(height: screenHeight / 100),
                  Text("Co-founder & CSO",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: beatDarkBlue,
                          fontFamily: 'Inter',
                          fontSize: screenHeight / 40,
                          fontWeight: FontWeight.w700)),
                  SizedBox(height: screenHeight / 100),
                  SizedBox(
                    width: screenWidth / 1.8,
                    child: Text(Team().lucilia,
                        textAlign: TextAlign.left,
                        maxLines: 20,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            height: 1.5,
                            color: beatDarkBlue,
                            fontFamily: 'Inter',
                            fontSize: screenHeight / 45,
                            fontWeight: FontWeight.w300)),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildMaria(double screenHeight, double screenWidth) {
    return Container(
      color: beatDarkBlue,
      width: screenWidth,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(left: screenWidth * 0.07),
            height: screenHeight / 2.7,
            width: screenWidth * 0.93,
            decoration: const BoxDecoration(
              gradient: RadialGradient(
                colors: [Color(0xff49AFDD), Color(0xff5ABFC9)],
              ),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(200),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("maria josé".toUpperCase(),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: Colors.white,
                          fontFamily: 'BigCaslon',
                          fontSize: screenHeight / 12,
                          fontWeight: FontWeight.w500)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/email_white.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(emailMaria);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                          const SizedBox(height: 5),
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/orcid_white.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(orcidMaria);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 10),
                      Text("u. ferreira".toUpperCase(),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              height: 1,
                              color: Colors.white,
                              fontFamily: 'BigCaslon',
                              fontSize: screenHeight / 12,
                              fontWeight: FontWeight.w500)),
                    ],
                  ),
                  SizedBox(height: screenHeight / 100),
                  Text("Co-founder & Head of Manufacturing",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: beatDarkBlue,
                          fontFamily: 'Inter',
                          fontSize: screenHeight / 40,
                          fontWeight: FontWeight.w700)),
                  SizedBox(height: screenHeight / 100),
                  SizedBox(
                    width: screenWidth / 1.9,
                    child: Text(Team().maria,
                        textAlign: TextAlign.left,
                        maxLines: 20,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            height: 1.5,
                            color: beatDarkBlue,
                            fontFamily: 'Inter',
                            fontSize: screenHeight / 45,
                            fontWeight: FontWeight.w300)),
                  ),
                ],
              ),
              SizedBox(
                width: screenWidth / 10,
              ),
              Container(
                  width: screenHeight / 2.7,
                  height: screenHeight / 2.7,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("images/profile_photo_majose.png"),
                      fit: BoxFit.cover,
                    ),
                  )),
              SizedBox(
                width: screenWidth / 40,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget buildLucio(double screenHeight, double screenWidth) {
    return Container(
      color: beatDarkBlue,
      width: screenWidth,
      height: screenHeight / 2.7,
      child: Stack(
        children: [
          Container(
            height: screenHeight / 2.6,
            width: screenWidth * 0.93,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(200),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth / 40,
              ),
              Container(
                  width: screenHeight / 2.7,
                  height: screenHeight / 2.7,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("images/profile_photo_lucio.png"),
                      fit: BoxFit.cover,
                    ),
                  )),
              SizedBox(
                width: screenWidth / 40,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("lúcio lara".toUpperCase(),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: beatMediumBlue,
                          fontFamily: 'BigCaslon',
                          fontSize: screenHeight / 12,
                          fontWeight: FontWeight.w500)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/email_blue.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(emailLucilia);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                          const SizedBox(height: 5),
                          Container(
                            height: screenHeight / 50,
                            width: screenHeight / 50,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("icons/orcid_blue.png"),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            child: ElevatedButton(
                              onPressed: () async {
                                await launchUrl(orcidLucio);
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent,
                                  surfaceTintColor: Colors.transparent),
                              child: Container(),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 10),
                      Text("santos".toUpperCase(),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              height: 1,
                              color: beatMediumBlue,
                              fontFamily: 'BigCaslon',
                              fontSize: screenHeight / 12,
                              fontWeight: FontWeight.w500)),
                    ],
                  ),
                  SizedBox(height: screenHeight / 100),
                  Text("Co-founder & Clinical Advisor",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          height: 1,
                          color: beatDarkBlue,
                          fontFamily: 'Inter',
                          fontSize: screenHeight / 40,
                          fontWeight: FontWeight.w700)),
                  SizedBox(height: screenHeight / 100),
                  SizedBox(
                    width: screenWidth / 1.8,
                    child: Text(Team().lucilia,
                        textAlign: TextAlign.left,
                        maxLines: 20,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            height: 1.5,
                            color: beatDarkBlue,
                            fontFamily: 'Inter',
                            fontSize: screenHeight / 45,
                            fontWeight: FontWeight.w300)),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildAngelaMobile(double screenHeight, double screenWidth) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("ângela".toUpperCase(),
            textAlign: TextAlign.left,
            style: TextStyle(
                height: 1,
                color: beatMediumBlue,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        Text("carvalho".toUpperCase(),
            textAlign: TextAlign.left,
            style: TextStyle(
                height: 1,
                color: beatMediumBlue,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        SizedBox(height: screenHeight / 100),
        Text("Co-founder & CEO",
            textAlign: TextAlign.left,
            style: TextStyle(
                height: 1,
                color: beatDarkBlue,
                fontFamily: 'Inter',
                fontSize: 22,
                fontWeight: FontWeight.w700)),
        SizedBox(height: screenHeight / 100),
        SizedBox(
          child: Text(Team().angela,
              textAlign: TextAlign.left,
              maxLines: 20,
              overflow: TextOverflow.clip,
              style: TextStyle(
                  height: 1.5,
                  color: beatDarkBlue,
                  fontFamily: 'Inter',
                  fontSize: 16,
                  fontWeight: FontWeight.w300)),
        ),
        SizedBox(height: screenHeight / 95),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: screenHeight / 42,
              width: screenHeight / 42,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/email_blue.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(emailAngela);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
            const SizedBox(width: 10),
            Container(
              height: screenHeight / 50,
              width: screenHeight / 50,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/linkedin_blue.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(linkedinAngela);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
          ],
        ),
        const SizedBox(height: 100),
      ],
    );
  }

  Widget buildHugoMobile(double screenHeight, double screenWidth) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const SizedBox(height: 30),
        Text("hugo".toUpperCase(),
            textAlign: TextAlign.right,
            style: const TextStyle(
                height: 1,
                color: Colors.white,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        Text("Prazeres".toUpperCase(),
            textAlign: TextAlign.right,
            style: const TextStyle(
                height: 1,
                color: Colors.white,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        SizedBox(height: screenHeight / 100),
        const Text("Co-founder & CBDO",
            textAlign: TextAlign.right,
            style: TextStyle(
                height: 1,
                color: Colors.white,
                fontFamily: 'Inter',
                fontSize: 22,
                fontWeight: FontWeight.w700)),
        SizedBox(height: screenHeight / 100),
        SizedBox(
          child: Text(Team().hugo,
              textAlign: TextAlign.right,
              maxLines: 20,
              overflow: TextOverflow.clip,
              style: const TextStyle(
                  height: 1.5,
                  color: Colors.white,
                  fontFamily: 'Inter',
                  fontSize: 16,
                  fontWeight: FontWeight.w300)),
        ),
        SizedBox(height: screenHeight / 95),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              height: screenHeight / 42,
              width: screenHeight / 42,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/email_white.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(emailHugo);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
            const SizedBox(width: 10),
            Container(
              height: screenHeight / 50,
              width: screenHeight / 50,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/linkedin_white.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(linkedinHugo);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
          ],
        ),
        const SizedBox(height: 100),
      ],
    );
  }

  Widget buildLuciliaMobile(double screenHeight, double screenWidth) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 30),
        Text("lucília".toUpperCase(),
            textAlign: TextAlign.left,
            style: TextStyle(
                height: 1,
                color: beatMediumBlue,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        Text("saraiva".toUpperCase(),
            textAlign: TextAlign.left,
            style: TextStyle(
                height: 1,
                color: beatMediumBlue,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        SizedBox(height: screenHeight / 100),
        Text("Co-founder & CSO",
            textAlign: TextAlign.left,
            style: TextStyle(
                height: 1,
                color: beatDarkBlue,
                fontFamily: 'Inter',
                fontSize: 22,
                fontWeight: FontWeight.w700)),
        SizedBox(height: screenHeight / 100),
        SizedBox(
          child: Text(Team().lucilia,
              textAlign: TextAlign.left,
              maxLines: 20,
              overflow: TextOverflow.clip,
              style: TextStyle(
                  height: 1.5,
                  color: beatDarkBlue,
                  fontFamily: 'Inter',
                  fontSize: 16,
                  fontWeight: FontWeight.w300)),
        ),
        SizedBox(height: screenHeight / 95),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: screenHeight / 42,
              width: screenHeight / 42,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/email_blue.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(emailLucilia);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
            const SizedBox(width: 10),
            Container(
              height: screenHeight / 50,
              width: screenHeight / 50,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/orcid_blue.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(orcidLucilia);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
          ],
        ),
        const SizedBox(height: 100),
      ],
    );
  }

  Widget buildMariaMobile(double screenHeight, double screenWidth) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const SizedBox(height: 30),
        Text("maria josé".toUpperCase(),
            textAlign: TextAlign.right,
            style: const TextStyle(
                height: 1,
                color: Colors.white,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        Text("u. prazeres".toUpperCase(),
            textAlign: TextAlign.right,
            style: const TextStyle(
                height: 1,
                color: Colors.white,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        SizedBox(height: screenHeight / 100),
        const Text("Co-founder &\nHead of Manufacturing",
            textAlign: TextAlign.right,
            style: TextStyle(
                height: 1,
                color: Colors.white,
                fontFamily: 'Inter',
                fontSize: 22,
                fontWeight: FontWeight.w700)),
        SizedBox(height: screenHeight / 100),
        SizedBox(
          child: Text(Team().maria,
              textAlign: TextAlign.right,
              maxLines: 20,
              overflow: TextOverflow.clip,
              style: const TextStyle(
                  height: 1.5,
                  color: Colors.white,
                  fontFamily: 'Inter',
                  fontSize: 16,
                  fontWeight: FontWeight.w300)),
        ),
        SizedBox(height: screenHeight / 95),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              height: screenHeight / 42,
              width: screenHeight / 42,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/email_white.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(emailMaria);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
            const SizedBox(width: 10),
            Container(
              height: screenHeight / 50,
              width: screenHeight / 50,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/orcid_white.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(orcidMaria);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
          ],
        ),
        const SizedBox(height: 100),
      ],
    );
  }

  Widget buildLucioMobile(double screenHeight, double screenWidth) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 30),
        Text("lúcio lara".toUpperCase(),
            textAlign: TextAlign.left,
            style: TextStyle(
                height: 1,
                color: beatMediumBlue,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        Text("santos".toUpperCase(),
            textAlign: TextAlign.left,
            style: TextStyle(
                height: 1,
                color: beatMediumBlue,
                fontFamily: 'BigCaslon',
                fontSize: 32,
                fontWeight: FontWeight.w500)),
        SizedBox(height: screenHeight / 100),
        Text("Co-founder & Clinical Advisor",
            textAlign: TextAlign.left,
            style: TextStyle(
                height: 1,
                color: beatDarkBlue,
                fontFamily: 'Inter',
                fontSize: 22,
                fontWeight: FontWeight.w700)),
        SizedBox(height: screenHeight / 100),
        SizedBox(
          child: Text(Team().lucio,
              textAlign: TextAlign.left,
              maxLines: 20,
              overflow: TextOverflow.clip,
              style: TextStyle(
                  height: 1.5,
                  color: beatDarkBlue,
                  fontFamily: 'Inter',
                  fontSize: 16,
                  fontWeight: FontWeight.w300)),
        ),
        SizedBox(height: screenHeight / 95),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: screenHeight / 42,
              width: screenHeight / 42,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/email_blue.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(emailLucio);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
            const SizedBox(width: 10),
            Container(
              height: screenHeight / 50,
              width: screenHeight / 50,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("icons/orcid_blue.png"),
                  fit: BoxFit.fitWidth,
                ),
              ),
              child: ElevatedButton(
                onPressed: () async {
                  await launchUrl(orcidLucio);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    surfaceTintColor: Colors.transparent),
                child: Container(),
              ),
            ),
          ],
        ),
        const SizedBox(height: 100),
      ],
    );
  }
}
