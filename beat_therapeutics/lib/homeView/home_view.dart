import 'package:beat_therapeutics/Navbar/custom_tab_bar.dart';
import 'package:beat_therapeutics/Navbar/responsive_top_navigation_bar.dart';
import 'package:beat_therapeutics/Others/extensions.dart';
import 'package:beat_therapeutics/Others/styles.dart';
import 'package:beat_therapeutics/homeView/contacts.dart';
import 'package:beat_therapeutics/homeView/first_triangle.dart';
import 'package:beat_therapeutics/homeView/science.dart';
import 'package:beat_therapeutics/homeView/second_triangle.dart';
import 'package:beat_therapeutics/homeView/team.dart';
import 'package:flutter/material.dart';
import 'package:simple_drawer/simple_drawer.dart';
import 'package:infinite_carousel/infinite_carousel.dart';

class HomepageView extends StatefulWidget {
  const HomepageView({super.key});

  @override
  State<HomepageView> createState() => _HomepageViewState();
}

class _HomepageViewState extends State<HomepageView>
    with SingleTickerProviderStateMixin {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  final tab1Key = GlobalKey();
  final tab2Key = GlobalKey();
  final tab3Key = GlobalKey();
  final tab4Key = GlobalKey();
  final tab5Key = GlobalKey();
  final ScrollController scrollController = ScrollController();
  late InfiniteScrollController controllerCarousel1;
  late InfiniteScrollController controllerCarousel2;

  int selectedIndex = 0;

  Color beatDarkBlue = const Color(0xff3A7193);
  Color beatMediumBlue = const Color(0xff49AFDE);
  Color beatLightBlue = const Color(0xff5ABFC9);
  late Color navbarColor;
  late Color navbarTextColor;
  late TabController tabController;
  late bool isHomepage;
  late bool drawerOpen;
  late bool hoverCarousel1;
  late bool hoverCarousel2;
  int page = 0;

  List<CustomTab> navbarItems = [
    const CustomTab(title: 'Home'),
    const CustomTab(title: 'About'),
    const CustomTab(title: 'Science'),
    const CustomTab(title: 'Team'),
    const CustomTab(title: 'Contacts'),
  ];

  final List<String> awardsImgList = [
    'images/awards/awards_0.png',
    'images/awards/awards_1.png',
    'images/awards/awards_2.png',
    'images/awards/awards_3.png',
    'images/awards/awards_4.png',
    'images/awards/awards_5.png',
    'images/awards/awards_7.png',
    'images/awards/awards_8.png',
    'images/awards/awards_9.png',
  ];

  final List<String> partnersImgList = [
    'images/partners/partners_0.jpg',
    'images/partners/partners_1.jpg',
    'images/partners/partners_2.jpg',
    'images/partners/partners_3.jpg',
    'images/partners/partners_4.jpg',
    'images/partners/partners_5.jpg',
    'images/partners/partners_6.jpg',
    'images/partners/partners_7.jpg',
  ];

  @override
  void initState() {
    navbarColor = Colors.transparent;
    navbarTextColor = beatDarkBlue;
    isHomepage = true;
    drawerOpen = false;
    hoverCarousel1 = false;
    hoverCarousel2 = false;
    controllerCarousel1 = InfiniteScrollController(initialItem: selectedIndex);
    controllerCarousel2 = InfiniteScrollController(initialItem: selectedIndex);
    tabController = TabController(length: navbarItems.length, vsync: this);
    scrollController.addListener(() {
      setState(() {
        if (MediaQuery.of(context).size.height >
            tab1Key.globalPaintBounds!.dy * 2) {
          tabController.index = 0;
        }
        if (MediaQuery.of(context).size.height >
            tab2Key.globalPaintBounds!.dy * 2) {
          tabController.index = 1;
        }
        if (MediaQuery.of(context).size.height >
            tab3Key.globalPaintBounds!.dy * 2) {
          tabController.index = 2;
        }
        if (MediaQuery.of(context).size.height >
            tab4Key.globalPaintBounds!.dy * 2) {
          tabController.index = 3;
        }
        if (MediaQuery.of(context).size.height >
            tab5Key.globalPaintBounds!.dy * 2) {
          tabController.index = 4;
        }
        if (tab1Key.globalPaintBounds!.dy >= -200) {
          navbarColor = Colors.transparent;
          navbarTextColor = beatDarkBlue;
          isHomepage = true;
        } else if (tab1Key.globalPaintBounds!.dy <= -200) {
          navbarColor = beatDarkBlue;
          navbarTextColor = Colors.white;
          isHomepage = false;
        } else {
          navbarColor = beatDarkBlue;
          navbarTextColor = Colors.white;
          isHomepage = false;
        }
      });
    });
    super.initState();
  }

  Widget logoContainerBlack() {
    final screenWidth = MediaQuery.of(context).size.width;
    return ElevatedButton(
      onPressed: () => Scrollable.ensureVisible(tab1Key.currentContext!,
          duration: const Duration(milliseconds: 1000)),
      style: ElevatedButton.styleFrom(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          surfaceTintColor: Colors.transparent),
      child: Container(
          width: screenWidth > 900 ? 250 : 155,
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/logo_black.png'),
                fit: BoxFit.fitWidth),
          )),
    );
  }

  Widget logoContainerWhite() {
    final screenWidth = MediaQuery.of(context).size.width;
    return ElevatedButton(
      onPressed: () => Scrollable.ensureVisible(tab1Key.currentContext!,
          duration: const Duration(milliseconds: 1000)),
      style: ElevatedButton.styleFrom(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          surfaceTintColor: Colors.transparent),
      child: Container(
          width: screenWidth > 900 ? 250 : 155,
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/logo_white.png'),
                fit: BoxFit.fitWidth),
          )),
    );
  }

  PreferredSizeWidget navbar() {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return ResponsiveTopNavigationBar(
      key1: tab1Key,
      key2: tab2Key,
      key3: tab3Key,
      key4: tab4Key,
      key5: tab5Key,
      drawerOpen: drawerOpen,
      context: context,
      navbarItems: navbarItems,
      screenHeight: screenHeight,
      height: screenWidth > 715 ? 90 : 55,
      backgroundColor: navbarColor,
      tabController: tabController,
      selectedLabelStyle: const TextStyle(
          fontFamily: 'Inter', fontSize: 22, fontWeight: FontWeight.w700),
      unselectedLabelStyle: const TextStyle(
          fontFamily: 'Inter', fontSize: 20, fontWeight: FontWeight.w500),
      indicatorColor: Colors.transparent,
      selectedLabelColor: navbarTextColor,
      unselectedLabelColor: navbarTextColor,
      onTapIconButton: () {},
      imageContainer: isHomepage ? logoContainerBlack() : logoContainerWhite(),
    );
  }

  Widget desktopView() {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    return Column(children: [
      SizedBox(
        width: screenWidth,
        height: screenHeight + 110,
        child: Stack(
          key: tab1Key,
          children: [
            Container(
                decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/backgroundImage.png"),
                fit: BoxFit.cover,
              ),
            )),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text('Targeting DNA\nDamage Response to\ntreat cancer',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: beatDarkBlue,
                            fontFamily: 'Inter',
                            fontSize: 92,
                            fontWeight: FontWeight.w600)),
                    const SizedBox(height: 400)
                  ],
                ),
                const SizedBox(width: 60)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        onPressed: () => Scrollable.ensureVisible(
                            tab2Key.currentContext!,
                            duration: const Duration(milliseconds: 1000)),
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            shadowColor: Colors.transparent,
                            surfaceTintColor: Colors.transparent),
                        child: Image.asset(
                          'icons/icon_arrow_down_rounded.png',
                          scale: 1.75,
                        )),
                    Container(
                      height: 290,
                      width: screenWidth,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Colors.white.withOpacity(0.0),
                            Colors.white.withOpacity(0.7),
                            Colors.white,
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
      SizedBox(
        height: 89,
        key: tab2Key,
      ),
      Container(
        width: screenWidth,
        color: Colors.white,
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FirstTriangle().desktop(screenHeight, screenWidth, tab3Key),
                Science().desktopScience(screenHeight, screenWidth)
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: screenHeight / 3,
                ),
                SecondTriangle().desktop(
                  screenHeight,
                  screenWidth,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: screenWidth / 13,
                      child: Text('First-in-class DDR inhibitor',
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              color: beatDarkBlue,
                              fontFamily: 'Inter',
                              fontSize: 50,
                              fontWeight: FontWeight.w600)),
                    ),
                    const SizedBox(
                      width: 100,
                    )
                  ],
                ),
                Science().desktopScienceIcons(screenHeight, screenWidth)
              ],
            ),
          ],
        ),
      ),
      Container(
          decoration: BoxDecoration(
              color: Colors.white, border: Border.all(color: Colors.white)),
          width: screenWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(key: tab4Key, height: 100),
              Text('Leadership'.toUpperCase(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: beatMediumBlue,
                      fontFamily: 'Inter',
                      fontSize: 75,
                      fontWeight: FontWeight.w800)),
              const SizedBox(height: 100),
              Team().buildAngela(screenHeight, screenWidth),
              Team().buildHugo(screenHeight, screenWidth),
              Team().buildLucilia(screenHeight, screenWidth),
              Team().buildMaria(screenHeight, screenWidth),
              Team().buildLucio(screenHeight, screenWidth)
            ],
          )),
      Container(
          width: screenWidth,
          decoration: BoxDecoration(
              color: Colors.white, border: Border.all(color: Colors.white)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 100),
              Text('funding & awards'.toUpperCase(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: beatMediumBlue,
                      fontFamily: 'Inter',
                      fontSize: 75,
                      fontWeight: FontWeight.w800)),
              const SizedBox(height: 75),
              MouseRegion(
                onEnter: (e) {
                  setState(() {
                    hoverCarousel1 = true;
                  });
                },
                onExit: (e) {
                  setState(() {
                    hoverCarousel1 = false;
                  });
                },
                child: Container(
                    height: 160,
                    padding: const EdgeInsets.symmetric(horizontal: 75),
                    child: Stack(
                      children: [
                        InfiniteCarousel.builder(
                          itemCount: awardsImgList.length,
                          itemExtent: 370,
                          center: true,
                          anchor: 0.0,
                          velocityFactor: 0.2,
                          onIndexChanged: (index) {
                            if (selectedIndex != index) {
                              setState(() {
                                selectedIndex = index;
                              });
                            }
                          },
                          controller: controllerCarousel1,
                          axisDirection: Axis.horizontal,
                          loop: true,
                          itemBuilder: (context, itemIndex, realIndex) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 30.0, vertical: 5),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white,
                                  boxShadow: kElevationToShadow[2],
                                  image: DecorationImage(
                                    image: AssetImage(awardsImgList[itemIndex]),
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                        hoverCarousel1
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              controllerCarousel1.nextItem();
                                            });
                                          },
                                          child: Container(
                                            height: 50,
                                            width: 25,
                                            color:
                                                Colors.grey.withOpacity(0.15),
                                            child: Image.asset(
                                              'icons/icon_arrow_left_rounded.png',
                                              scale: 3,
                                            ),
                                          )),
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              controllerCarousel1.nextItem();
                                            });
                                          },
                                          child: Container(
                                            height: 50,
                                            width: 25,
                                            color:
                                                Colors.grey.withOpacity(0.15),
                                            child: Image.asset(
                                              'icons/icon_arrow_right_rounded.png',
                                              scale: 3,
                                            ),
                                          )),
                                    ],
                                  ),
                                ],
                              )
                            : Container()
                      ],
                    )),
              ),
              const SizedBox(height: 100),
              Text('Network'.toUpperCase(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: beatMediumBlue,
                      fontFamily: 'Inter',
                      fontSize: 75,
                      fontWeight: FontWeight.w800)),
              const SizedBox(height: 75),
              MouseRegion(
                onEnter: (e) {
                  setState(() {
                    hoverCarousel2 = true;
                  });
                },
                onExit: (e) {
                  setState(() {
                    hoverCarousel2 = false;
                  });
                },
                child: Container(
                    height: 160,
                    padding: const EdgeInsets.symmetric(horizontal: 75),
                    child: Stack(
                      children: [
                        InfiniteCarousel.builder(
                          itemCount: partnersImgList.length,
                          itemExtent: 370,
                          center: true,
                          velocityFactor: 0.2,
                          onIndexChanged: (index) {
                            if (selectedIndex != index) {
                              setState(() {
                                selectedIndex = index;
                              });
                            }
                          },
                          controller: controllerCarousel2,
                          axisDirection: Axis.horizontal,
                          loop: true,
                          itemBuilder: (context, itemIndex, realIndex) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 30.0, vertical: 5),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white,
                                  boxShadow: kElevationToShadow[2],
                                  image: DecorationImage(
                                    image:
                                        AssetImage(partnersImgList[itemIndex]),
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                        hoverCarousel2
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              controllerCarousel2.nextItem();
                                            });
                                          },
                                          child: Container(
                                            height: 50,
                                            width: 25,
                                            color:
                                                Colors.grey.withOpacity(0.15),
                                            child: Image.asset(
                                              'icons/icon_arrow_left_rounded.png',
                                              scale: 3,
                                            ),
                                          )),
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              controllerCarousel2.nextItem();
                                            });
                                          },
                                          child: Container(
                                            height: 50,
                                            width: 25,
                                            color:
                                                Colors.grey.withOpacity(0.15),
                                            child: Image.asset(
                                              'icons/icon_arrow_right_rounded.png',
                                              scale: 3,
                                            ),
                                          )),
                                    ],
                                  ),
                                ],
                              )
                            : Container()
                      ],
                    )),
              ),
              const SizedBox(height: 200)
            ],
          )),
      Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: Colors.white, border: Border.all(color: Colors.white)),
          child: Contacts().contactsWeb(tab5Key))
    ]);
  }

  Widget mobileView() {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    return Container(
      color: Colors.white,
      child: Column(children: [
        SizedBox(
          width: screenWidth,
          height: screenHeight + 50,
          child: Stack(
            key: tab1Key,
            children: [
              Container(
                  decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/backgroundImage.png"),
                  fit: BoxFit.cover,
                ),
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                          'Targeting\nDNA Damage\nResponse\nto treat\ncancer'
                              .toUpperCase(),
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              color: beatDarkBlue,
                              fontFamily: 'Inter',
                              fontSize: 40,
                              fontWeight: FontWeight.w600)),
                      const SizedBox(height: 150)
                    ],
                  ),
                  const SizedBox(width: 10)
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ElevatedButton(
                      onPressed: () => Scrollable.ensureVisible(
                          tab2Key.currentContext!,
                          duration: const Duration(milliseconds: 1000)),
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.transparent,
                          shadowColor: Colors.transparent,
                          surfaceTintColor: Colors.transparent),
                      child: Image.asset(
                        'icons/icon_arrow_down_rounded.png',
                        scale: 2.5,
                      )),
                  Container(
                    key: tab2Key,
                    height: 150,
                    width: screenWidth,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.white.withOpacity(0.0),
                          Colors.white.withOpacity(0.7),
                          Colors.white,
                        ],
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        Column(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: beatDarkBlue,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  const SizedBox(height: 25),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('who\nwe\nare'.toUpperCase(),
                          textAlign: TextAlign.left,
                          style: const TextStyle(
                              height: 1.1,
                              color: Colors.white,
                              fontFamily: 'BigCaslon',
                              fontSize: 40,
                              fontWeight: FontWeight.w500)),
                      const SizedBox(width: 5),
                      Container(
                          width: 130,
                          height: 130,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("images/microscope.png"),
                              fit: BoxFit.cover,
                            ),
                          )),
                    ],
                  ),
                  const SizedBox(height: 40),
                  FirstTriangle().mobile(screenWidth),
                  const SizedBox(height: 40),
                ],
              ),
            ),
            Container(
              height: 30,
              width: screenWidth,
              decoration: const BoxDecoration(color: Colors.white),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: beatLightBlue,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  const SizedBox(height: 40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: 130,
                          height: 130,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("images/cell.png"),
                              fit: BoxFit.cover,
                            ),
                          )),
                      const SizedBox(width: 5),
                      Text('Our\nMission'.toUpperCase(),
                          textAlign: TextAlign.left,
                          style: const TextStyle(
                              height: 1.1,
                              color: Colors.white,
                              fontFamily: 'BigCaslon',
                              fontSize: 40,
                              fontWeight: FontWeight.w500)),
                    ],
                  ),
                  const SizedBox(height: 40),
                  SecondTriangle().mobile(screenWidth),
                  const SizedBox(height: 40),
                ],
              ),
            ),
            Container(
              key: tab3Key,
              height: 30,
              width: screenWidth,
              decoration: const BoxDecoration(color: Colors.white),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: const RadialGradient(
                  colors: [Color(0xff49AFDD), Color(0xff5ABFC9)],
                ),
              ),
              child: Column(
                children: [
                  const SizedBox(height: 40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(width: 35),
                      Text('science'.toUpperCase(),
                          textAlign: TextAlign.left,
                          style: const TextStyle(
                              color: Colors.white,
                              fontFamily: 'BigCaslon',
                              fontSize: 45,
                              fontWeight: FontWeight.w500)),
                    ],
                  ),
                  const SizedBox(height: 40),
                  Science().mobile(screenWidth),
                  const SizedBox(height: 60),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: 130,
                          height: 160,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("icons/IP_protected.png"),
                              fit: BoxFit.fitHeight,
                            ),
                          )),
                      const Text('IP Protected',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Inter',
                              fontSize: 20,
                              fontWeight: FontWeight.w500))
                    ],
                  ),
                  const SizedBox(height: 40),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              width: screenWidth,
              child: Column(children: [
                const SizedBox(height: 20),
                SizedBox(
                  child: Text('First-in-class\nDDR inhibitor',
                      textAlign: TextAlign.right,
                      style: TextStyle(
                          color: beatDarkBlue,
                          fontFamily: 'Inter',
                          fontSize: 30,
                          fontWeight: FontWeight.w600)),
                ),
                const SizedBox(height: 20),
                Column(
                  children: [
                    Container(
                        width: 150,
                        height: 150,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("icons/science_1.png"),
                            fit: BoxFit.fitWidth,
                          ),
                        )),
                    const SizedBox(height: 10),
                    Text('Small\nMolecules',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: beatDarkBlue,
                            fontFamily: 'Inter',
                            fontSize: 20,
                            fontWeight: FontWeight.w400))
                  ],
                ),
                const SizedBox(height: 40),
                Column(
                  children: [
                    Container(
                        width: 150,
                        height: 150,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("icons/science_2.png"),
                            fit: BoxFit.fitWidth,
                          ),
                        )),
                    const SizedBox(height: 10),
                    Text('Synthetic\nLethality',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: beatDarkBlue,
                            fontFamily: 'Inter',
                            fontSize: 20,
                            fontWeight: FontWeight.w400))
                  ],
                ),
                const SizedBox(height: 40),
                Column(
                  children: [
                    Container(
                        width: 150,
                        height: 150,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("icons/science_3.png"),
                            fit: BoxFit.fitHeight,
                          ),
                        )),
                    const SizedBox(height: 10),
                    Text('Overpower\nResistance',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: beatDarkBlue,
                            fontFamily: 'Inter',
                            fontSize: 20,
                            fontWeight: FontWeight.w400))
                  ],
                )
              ]),
            )
          ],
        ),
        Container(
            key: tab4Key,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white, border: Border.all(color: Colors.white)),
            child: Center(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 90),
                Text('leadership'.toUpperCase(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: beatMediumBlue,
                        fontFamily: 'Inter',
                        fontSize: 40,
                        fontWeight: FontWeight.w800)),
                Container(
                  color: beatDarkBlue,
                  width: screenWidth,
                  child: Column(
                    children: [
                      Container(
                        color: Colors.white,
                        height: 100,
                        width: screenWidth,
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(200),
                          ),
                        ),
                        width: screenWidth,
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child:
                            Team().buildAngelaMobile(screenHeight, screenWidth),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          gradient: RadialGradient(
                            colors: [Color(0xff49AFDD), Color(0xff5ABFC9)],
                          ),
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(200),
                          ),
                        ),
                        width: screenWidth,
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child:
                            Team().buildHugoMobile(screenHeight, screenWidth),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(200),
                          ),
                        ),
                        width: screenWidth,
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: Team()
                            .buildLuciliaMobile(screenHeight, screenWidth),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          gradient: RadialGradient(
                            colors: [Color(0xff49AFDD), Color(0xff5ABFC9)],
                          ),
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(200),
                          ),
                        ),
                        width: screenWidth,
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child:
                            Team().buildMariaMobile(screenHeight, screenWidth),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(200),
                          ),
                        ),
                        width: screenWidth,
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child:
                            Team().buildLucioMobile(screenHeight, screenWidth),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 90),
                Text('funding &\nawards'.toUpperCase(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: beatMediumBlue,
                        fontFamily: 'Inter',
                        fontSize: 40,
                        fontWeight: FontWeight.w800)),
                const SizedBox(height: 30),
                Container(
                    height: 70,
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: InfiniteCarousel.builder(
                      itemCount: awardsImgList.length,
                      itemExtent: 150,
                      center: true,
                      anchor: 0.0,
                      velocityFactor: 0.4,
                      onIndexChanged: (index) {
                        if (selectedIndex != index) {
                          setState(() {
                            selectedIndex = index;
                          });
                        }
                      },
                      controller: controllerCarousel1,
                      axisDirection: Axis.horizontal,
                      loop: true,
                      itemBuilder: (context, itemIndex, realIndex) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: GestureDetector(
                            onTap: () {},
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white,
                                boxShadow: kElevationToShadow[2],
                                image: DecorationImage(
                                  image: AssetImage(awardsImgList[itemIndex]),
                                  fit: BoxFit.fitWidth,
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    )),
                const SizedBox(height: 50),
                Text('Network'.toUpperCase(),
                    style: TextStyle(
                        color: beatMediumBlue,
                        fontFamily: 'Inter',
                        fontSize: 40,
                        fontWeight: FontWeight.w800)),
                const SizedBox(height: 30),
                Container(
                    height: 70,
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: InfiniteCarousel.builder(
                      itemCount: partnersImgList.length,
                      itemExtent: 150,
                      center: true,
                      anchor: 0.0,
                      velocityFactor: 0.4,
                      onIndexChanged: (index) {
                        if (selectedIndex != index) {
                          setState(() {
                            selectedIndex = index;
                          });
                        }
                      },
                      controller: controllerCarousel2,
                      axisDirection: Axis.horizontal,
                      loop: true,
                      itemBuilder: (context, itemIndex, realIndex) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: GestureDetector(
                            onTap: () {},
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white,
                                boxShadow: kElevationToShadow[2],
                                image: DecorationImage(
                                  image: AssetImage(partnersImgList[itemIndex]),
                                  fit: BoxFit.fitWidth,
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    )),
              ],
            ))),
        const SizedBox(height: 50),
        Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white, border: Border.all(color: Colors.white)),
            child: Contacts().contactsMobile(tab5Key))
      ]),
    );
  }

  Widget topNavigationDrawer() {
    return SimpleDrawer(
      childHeight: 460,
      direction: Direction.top,
      onDrawerStatusChanged: (status) {
        if (status.toString() == 'DrawerStatus.inactive') {
          setState(() {
            drawerOpen = false;
          });
        }
        if (status.toString() == 'DrawerStatus.active') {
          setState(() {
            drawerOpen = true;
          });
        }
      },
      id: "top",
      child: Container(
        color: tabController.index == 0 ? Colors.white : beatDarkBlue,
        width: MediaQuery.of(context).size.width,
        height: 460,
        child: ListView(
          children: [
            SizedBox(
              height: 80,
              child: ElevatedButton(
                  style: Styles().drawerButtons(tabController),
                  onPressed: () {
                    setState(() {
                      tabController.index = 0;
                      Scrollable.ensureVisible(tab1Key.currentContext!,
                          duration: const Duration(milliseconds: 1000));
                      SimpleDrawer.deactivate("top");
                    });
                  },
                  child: const Text("Home")),
            ),
            SizedBox(
              height: 80,
              child: ElevatedButton(
                  style: Styles().drawerButtons(tabController),
                  onPressed: () {
                    setState(() {
                      tabController.index = 1;
                      Scrollable.ensureVisible(tab2Key.currentContext!,
                          duration: const Duration(milliseconds: 1000));
                      SimpleDrawer.deactivate("top");
                    });
                  },
                  child: const Text("About")),
            ),
            SizedBox(
              height: 80,
              child: ElevatedButton(
                  style: Styles().drawerButtons(tabController),
                  onPressed: () {
                    setState(() {
                      tabController.index = 2;
                      Scrollable.ensureVisible(tab3Key.currentContext!,
                          duration: const Duration(milliseconds: 1000));
                      SimpleDrawer.deactivate("top");
                    });
                  },
                  child: const Text("Science")),
            ),
            SizedBox(
              height: 80,
              child: ElevatedButton(
                  style: Styles().drawerButtons(tabController),
                  onPressed: () {
                    setState(() {
                      tabController.index = 3;
                      Scrollable.ensureVisible(tab4Key.currentContext!,
                          duration: const Duration(milliseconds: 1000));
                      SimpleDrawer.deactivate("top");
                    });
                  },
                  child: const Text("Team")),
            ),
            SizedBox(
              height: 80,
              child: ElevatedButton(
                  style: Styles().drawerButtons(tabController),
                  onPressed: () {
                    setState(() {
                      tabController.index = 4;
                      Scrollable.ensureVisible(tab5Key.currentContext!,
                          duration: const Duration(milliseconds: 1000));
                      SimpleDrawer.deactivate("top");
                      SimpleDrawer.deactivate("top");
                    });
                  },
                  child: const Text("Contacts")),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    /* print(MediaQuery.of(context).size.width);
    print(MediaQuery.of(context).size.height); */
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: beatDarkBlue,
        extendBodyBehindAppBar: true,
        appBar: navbar(),
        body: Container(
          color: Colors.white,
          height: double.infinity,
          child: Stack(
            children: [
              SingleChildScrollView(
                controller: scrollController,
                child: MediaQuery.of(context).size.width > 1000
                    ? desktopView()
                    : mobileView(),
              ),
              topNavigationDrawer()
            ],
          ),
        ));
  }
}
