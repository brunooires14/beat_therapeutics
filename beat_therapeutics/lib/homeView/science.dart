import 'package:beat_therapeutics/Others/extensions.dart';
import 'package:flutter/material.dart';

class Science {
  String descriptionMobile =
      "We leverage the DNA Damage Response (DDR) to unlock new therapeutic opportunities for cancer patients. Our DDR inhibition approach specifically targets homologous recombination, disrupting DNA repair mechanisms in cancer cells. This targeted strategy aims to maximize therapeutic efficacy while minimizing effects on healthy tissues.";

  String descriptionDesktop =
      "We leverage the DNA Damage Response (DDR) to unlock\nnew therapeutic opportunities for cancer patients.\nOur DDR inhibition approach specifically targets\nhomologous recombination, disrupting DNA repair\nmechanisms in cancer cells. This targeted strategy\naims to maximize therapeutic efficacy while\nminimizing effects on healthy tissues.";

  Widget clipPathQuarterCircle(
      double screenHeight,
      double screenWidth,
      double titleSize,
      double textSize,
      double firstSpacerHeight,
      double secondSpacerHeight,
      double thirdSpacerHeight,
      double imageWidth,
      double imageHeight,
      double imageTextSize) {
    return ClipPath(
      clipper: QuarterCircleClipper(),
      child: Container(
        decoration: const BoxDecoration(
          gradient: RadialGradient(
            colors: [Color(0xff49AFDD), Color(0xff5ABFC9)],
          ),
        ),
        height: screenHeight - 90,
        width: screenWidth * 0.65,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(
              width: 100,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: firstSpacerHeight),
                Text('Science'.toUpperCase(),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'BigCaslon',
                        fontSize: titleSize,
                        fontWeight: FontWeight.w500)),
                SizedBox(height: secondSpacerHeight),
                Text(descriptionDesktop,
                    textAlign: TextAlign.left,
                    softWrap: true,
                    maxLines: 7,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Inter',
                        fontSize: textSize,
                        height: 1.6,
                        fontWeight: FontWeight.w200)),
                SizedBox(height: thirdSpacerHeight),
                Row(
                  children: [
                    const SizedBox(width: 20),
                    Container(
                        width: imageWidth,
                        height: imageHeight,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("icons/IP_protected.png"),
                            fit: BoxFit.fitHeight,
                          ),
                        )),
                    Text('IP Protected',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Inter',
                            fontSize: imageTextSize,
                            fontWeight: FontWeight.w500))
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget scienceIcons(double screenHeight, double screenWidth, double imageSize,
      double textSize, double spaceBetweenImages, double marginRight) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Column(
          children: [
            Container(
                width: imageSize,
                height: imageSize,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("icons/science_1.png"),
                    fit: BoxFit.fitWidth,
                  ),
                )),
            const SizedBox(height: 30),
            Text('Small\nMolecules',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: const Color(0xff3A7193),
                    fontFamily: 'Inter',
                    fontSize: textSize,
                    fontWeight: FontWeight.w400))
          ],
        ),
        SizedBox(width: spaceBetweenImages),
        Column(
          children: [
            Container(
                width: imageSize,
                height: imageSize,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("icons/science_2.png"),
                    fit: BoxFit.fitWidth,
                  ),
                )),
            const SizedBox(height: 30),
            Text('Synthetic\nLethality',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: const Color(0xff3A7193),
                    fontFamily: 'Inter',
                    fontSize: textSize,
                    fontWeight: FontWeight.w400))
          ],
        ),
        SizedBox(width: spaceBetweenImages),
        Column(
          children: [
            Container(
                width: imageSize,
                height: imageSize,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("icons/science_3.png"),
                    fit: BoxFit.fitHeight,
                  ),
                )),
            const SizedBox(height: 30),
            Text('Overpowering\nResistance',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: const Color(0xff3A7193),
                    fontFamily: 'Inter',
                    fontSize: textSize,
                    fontWeight: FontWeight.w400))
          ],
        ),
        SizedBox(width: marginRight),
      ],
    );
  }

  Widget desktopScience(double screenHeight, double screenWidth) {
    if (screenHeight < 1150) {
      return clipPathQuarterCircle(screenHeight, screenWidth, screenHeight / 15,
          screenWidth / 70, 50, 50, 125, 140, 180, 35);
    }
    if (screenHeight > 1150 && screenHeight < 1300 && screenWidth < 1850) {
      return clipPathQuarterCircle(screenHeight, screenWidth, screenHeight / 15,
          screenWidth / 70, 50, 50, 125, 160, 200, 35);
    }
    if (screenHeight > 1150 && screenHeight < 1300 && screenWidth > 1850) {
      return clipPathQuarterCircle(screenHeight, screenWidth, screenHeight / 15,
          screenWidth / 70, 50, 50, 125, 180, 220, 45);
    }
    if (screenHeight > 1300 && screenHeight < 1400 && screenWidth > 1850) {
      return clipPathQuarterCircle(screenHeight, screenWidth, screenHeight / 15,
          screenWidth / 70, 50, 50, 125, 190, 230, 45);
    }
    if (screenHeight < 1500 && screenWidth < 2100) {
      return clipPathQuarterCircle(screenHeight, screenWidth, screenHeight / 15,
          screenWidth / 70, 50, 50, 125, 150, 190, 40);
    }
    return clipPathQuarterCircle(screenHeight, screenWidth, screenHeight / 15,
        screenWidth / 70, 50, 50, 150, 210, 250, 55);
  }

  Widget desktopScienceIcons(double screenHeight, double screenWidth) {
    if (screenHeight < 1150) {
      return scienceIcons(screenHeight, screenWidth, screenWidth / 12,
          screenWidth / 50, 60, 80);
    }
    if (screenHeight > 1150 && screenHeight < 1300 && screenWidth < 1850) {
      return scienceIcons(screenHeight, screenWidth, screenWidth / 11,
          screenWidth / 50, 50, 60);
    }
    return scienceIcons(
        screenHeight, screenWidth, screenWidth / 11, screenWidth / 50, 80, 100);
  }

  Widget mobile(double screenWidth) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(width: 10),
        SizedBox(
          width: screenWidth - 100,
          child: Text(descriptionMobile,
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              maxLines: 20,
              style: const TextStyle(
                  color: Colors.white,
                  fontFamily: 'Inter',
                  fontSize: 17,
                  fontWeight: FontWeight.w200)),
        ),
      ],
    );
  }
}
