import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Contacts {
  Color beatDarkBlue = const Color(0xff3A7193);

  Uri email = Uri.parse("mailto:info@beattherapeutics.com");
  Uri address = Uri.parse("https://maps.app.goo.gl/sxvjVQ7DMVDfaYrF6");
  Uri linkedinLink =
      Uri.parse("https://www.linkedin.com/company/beat-therapeutics/");

  Widget contactsWeb(GlobalKey tab5Key) {
    return Center(
        child: Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 85),
          key: tab5Key,
          height: 425,
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: RadialGradient(
              colors: [Color(0xff49AFDD), Color(0xff5ABFC9)],
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text('contacts'.toUpperCase(),
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          color: Colors.white,
                          fontFamily: 'Inter',
                          fontSize: 70,
                          fontWeight: FontWeight.w800))
                ],
              ),
              const SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "address".toUpperCase(),
                        style: const TextStyle(
                            color: Colors.white,
                            fontFamily: "Inter",
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextButton(
                        onPressed: () async {
                          await launchUrl(address);
                        },
                        style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                            alignment: Alignment.centerLeft),
                        child: Text(
                          "rua alfredo allen 455/461 4200-135 porto, portugal"
                              .toUpperCase(),
                          style: const TextStyle(
                              color: Colors.white,
                              fontFamily: "Inter",
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "e-mail".toUpperCase(),
                        style: const TextStyle(
                            color: Colors.white,
                            fontFamily: "Inter",
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextButton(
                        onPressed: () async {
                          await launchUrl(email);
                        },
                        style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                            alignment: Alignment.centerLeft),
                        child: Text(
                          "info@beattherapeutics.com".toUpperCase(),
                          style: const TextStyle(
                              color: Colors.white,
                              fontFamily: "Inter",
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "follow us".toUpperCase(),
                        style: const TextStyle(
                            color: Colors.white,
                            fontFamily: "Inter",
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextButton(
                        onPressed: () async {
                          await launchUrl(linkedinLink);
                        },
                        style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                            alignment: Alignment.centerLeft),
                        child: Text(
                          "linkedin.com/company/beat-therapeutics/"
                              .toUpperCase(),
                          style: const TextStyle(
                              color: Colors.white,
                              fontFamily: "Inter",
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          height: 100,
          width: double.infinity,
          color: beatDarkBlue,
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const SizedBox(width: 50),
                          Text(
                            "developed by ".toUpperCase(),
                            style: const TextStyle(
                                color: Colors.white,
                                fontFamily: "Inter",
                                fontSize: 23,
                                fontWeight: FontWeight.w400),
                          ),
                          Text(
                            "bruno sério".toUpperCase(),
                            style: const TextStyle(
                                color: Colors.white,
                                fontFamily: "Inter",
                                fontSize: 23,
                                fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "© Beat Therapeutics 2024. All rights reserved"
                                .toUpperCase(),
                            style: const TextStyle(
                                color: Colors.white,
                                fontFamily: "Inter",
                                fontSize: 23,
                                fontWeight: FontWeight.w600),
                          ),
                          const SizedBox(width: 50),
                        ],
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        )
      ],
    ));
  }

  Widget contactsMobile(GlobalKey tab5Key) {
    return Center(
        child: Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          key: tab5Key,
          height: 310,
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: RadialGradient(
              colors: [Color(0xff49AFDD), Color(0xff5ABFC9)],
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('contacts'.toUpperCase(),
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          color: Colors.white,
                          fontFamily: 'Inter',
                          fontSize: 25,
                          fontWeight: FontWeight.w800))
                ],
              ),
              const SizedBox(height: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "address".toUpperCase(),
                    style: const TextStyle(
                        color: Colors.white,
                        fontFamily: "Inter",
                        fontSize: 13,
                        fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  TextButton(
                    onPressed: () async {
                      await launchUrl(address);
                    },
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                        alignment: Alignment.centerLeft),
                    child: Text(
                      "rua alfredo allen 455/461\n4200-135 porto, portugal"
                          .toUpperCase(),
                      style: const TextStyle(
                          color: Colors.white,
                          fontFamily: "Inter",
                          fontSize: 13,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    "e-mail".toUpperCase(),
                    style: const TextStyle(
                        color: Colors.white,
                        fontFamily: "Inter",
                        fontSize: 13,
                        fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  TextButton(
                    onPressed: () async {
                      await launchUrl(email);
                    },
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                        alignment: Alignment.centerLeft),
                    child: Text(
                      "info@beattherapeutics.com".toUpperCase(),
                      style: const TextStyle(
                          color: Colors.white,
                          fontFamily: "Inter",
                          fontSize: 13,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    "follow us".toUpperCase(),
                    style: const TextStyle(
                        color: Colors.white,
                        fontFamily: "Inter",
                        fontSize: 13,
                        fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  TextButton(
                    onPressed: () async {
                      await launchUrl(linkedinLink);
                    },
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                        alignment: Alignment.centerLeft),
                    child: Text(
                      "linkedin.com/company/beat-therapeutics/".toUpperCase(),
                      style: const TextStyle(
                          color: Colors.white,
                          fontFamily: "Inter",
                          fontSize: 13,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          height: 120,
          width: double.infinity,
          color: beatDarkBlue,
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "© Beat Therapeutics 2024.\nAll rights reserved"
                        .toUpperCase(),
                    style: const TextStyle(
                        color: Colors.white,
                        fontFamily: "Inter",
                        fontSize: 13,
                        fontWeight: FontWeight.w600),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "developed by ".toUpperCase(),
                        style: const TextStyle(
                            color: Colors.white,
                            fontFamily: "Inter",
                            fontSize: 13,
                            fontWeight: FontWeight.w400),
                      ),
                      Text(
                        "bruno sério".toUpperCase(),
                        style: const TextStyle(
                            color: Colors.white,
                            fontFamily: "Inter",
                            fontSize: 13,
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
        )
      ],
    ));
  }
}
