import 'package:beat_therapeutics/Others/extensions.dart';
import 'package:flutter/material.dart';

class SecondTriangle {
  String descriptionDesktop =
      "BEAT Therapeutics is dedicated to improving the outcomes of cancer patients.\nWe are developing first-in-class therapies designed to selectively impact\ncancer cell, addressing the needs of patients facing hard-to-treat cancers\nand resistance to therapy. Based on cutting-edge research, and with a\nsteadfast commitment to patient well-being, we strive to set new\nbenchmarks in treatment efficacy, minimize side effects,\nand foster a healthier future.";
  String descriptionMobile =
      "BEAT Therapeutics is dedicated to improving the outcomes of cancer patients. We are developing first-in-class therapies designed to selectively impact cancer cells, addressing the needs of patients facing hard-to-treat cancers and resistance to therapy. Based on cutting-edge research, and with a steadfast commitment to patient well-being, we strive to set new benchmarks in treatment efficacy, minimize side effects, and foster a healthier future.";

  Widget clipPathSecondTriangle(
    double screenHeight,
    double screenWidth,
    double titleSize,
    double textSize,
    double firstSpacerHeight,
    double secondSpacerHeight,
    double imageWidth,
    double imageHeight,
    double imagePosRight,
    double imagePosTop,
    double titlePosRight,
    double titlePosTop,
  ) {
    return Stack(
      children: [
        ClipPath(
          clipper: SecondTriangleClipper(),
          child: Container(
            decoration: const BoxDecoration(
              gradient: RadialGradient(
                colors: [Color(0xff49AFDD), Color(0xff5ABFC9)],
              ),
            ),
            height: screenHeight - 95,
            width: screenWidth * 0.65,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SizedBox(height: firstSpacerHeight),
                    Text(descriptionDesktop,
                        textAlign: TextAlign.right,
                        softWrap: true,
                        maxLines: 7,
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Inter',
                            height: 1.6,
                            fontSize: textSize,
                            fontWeight: FontWeight.w200)),
                    const SizedBox(height: 20),
                  ],
                ),
                SizedBox(
                  width: secondSpacerHeight,
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: imagePosTop,
          right: imagePosRight,
          child: Container(
              width: imageWidth / 1.1,
              height: imageHeight / 1.1,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/cell.png"),
                  fit: BoxFit.cover,
                ),
              )),
        ),
        Positioned(
            top: titlePosTop,
            right: titlePosRight,
            child: Text('our\nmission'.toUpperCase(),
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'BigCaslon',
                    fontSize: titleSize,
                    height: 1.1,
                    fontWeight: FontWeight.w500))),
      ],
    );
  }

  Widget desktop(double screenHeight, double screenWidth) {
    if (screenHeight < 1150 && screenWidth < 1850) {
      return clipPathSecondTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 95,
          screenHeight / 20,
          screenHeight / 10,
          screenHeight / 2.6,
          screenHeight / 2.6,
          screenHeight / 5,
          screenHeight / 3.5,
          screenHeight / 10,
          screenHeight / 3);
    }
    if (screenHeight < 1150 && screenWidth > 1850) {
      return clipPathSecondTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 90,
          screenHeight / 20,
          screenWidth / 20,
          screenHeight / 2.6,
          screenHeight / 2.6,
          screenHeight / 4,
          screenHeight / 2.5,
          screenHeight / 10,
          screenHeight / 2.5);
    }
    if (screenHeight > 1150 && screenHeight < 1300 && screenWidth < 2200) {
      return clipPathSecondTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 85,
          screenHeight / 18,
          screenHeight / 14,
          screenHeight / 2.4,
          screenHeight / 2.4,
          screenHeight / 4,
          screenHeight / 3,
          screenHeight / 10,
          screenHeight / 3);
    }
    if (screenHeight > 1150 && screenHeight < 1300 && screenWidth > 2200) {
      return clipPathSecondTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 80,
          screenWidth / 40,
          screenWidth / 45,
          screenHeight / 2.4,
          screenHeight / 2.4,
          screenHeight / 4,
          screenHeight / 2.5,
          screenHeight / 10,
          screenHeight / 2.5);
    }
    if (screenHeight > 1300 && screenHeight < 1400 && screenWidth > 2600) {
      return clipPathSecondTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 75,
          screenWidth / 45,
          screenWidth / 35,
          screenHeight / 2.4,
          screenHeight / 2.4,
          screenHeight / 4,
          screenHeight / 2.5,
          screenHeight / 10,
          screenHeight / 2.5);
    }
    if (screenHeight > 1300 && screenHeight < 1400 && screenWidth < 2600) {
      return clipPathSecondTriangle(
          screenHeight,
          screenWidth,
          screenHeight / 15,
          screenWidth / 75,
          screenWidth / 45,
          screenWidth / 30,
          screenHeight / 2.4,
          screenHeight / 2.4,
          screenHeight / 4,
          screenHeight / 2.5,
          screenHeight / 10,
          screenHeight / 2.5);
    }
    return clipPathSecondTriangle(
        screenHeight,
        screenWidth,
        screenHeight / 15,
        screenWidth / 75,
        screenWidth / 45,
        screenWidth / 30,
        screenHeight / 2.4,
        screenHeight / 2.4,
        screenHeight / 4,
        screenHeight / 2.5,
        screenHeight / 10,
        screenHeight / 2.5);
  }

  Widget mobile(double screenWidth) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(width: 10),
        Column(
          children: [
            SizedBox(
              width: screenWidth - 100,
              child: Text(descriptionMobile,
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 20,
                  style: const TextStyle(
                      color: Colors.white,
                      fontFamily: 'Inter',
                      fontSize: 17,
                      fontWeight: FontWeight.w200)),
            )
          ],
        ),
      ],
    );
  }
}
